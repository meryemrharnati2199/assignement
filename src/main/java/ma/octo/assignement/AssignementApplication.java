package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private VirementRepository virementRepository;
	@Autowired
	private VersementRepository versementRepository;

	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		Utilisateur userEmetteur = new Utilisateur("user1", "last1", "first1", "Male");
		/*utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");*/

		utilisateurRepository.save(userEmetteur);


		Utilisateur userBeneficiaire = new Utilisateur("user2", "last2", "first2", "Female");
		/*utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");*/

		utilisateurRepository.save(userBeneficiaire);

		Compte compteEmetteur = new Compte("010000A000001000", "RIB1", BigDecimal.valueOf(200000L), userEmetteur);
		/*compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);*/
		

		compteRepository.save(compteEmetteur);

		Compte compteBeneficiaire = new Compte("010000B025001000", "RIB2", BigDecimal.valueOf(140000L), userBeneficiaire);
		/*compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);*/

		compteRepository.save(compteBeneficiaire);

		Virement virement = new Virement(new Date(), compteBeneficiaire, compteEmetteur, BigDecimal.TEN, "Assignement 2021");
		/*v.setMontantVirement(BigDecimal.TEN);
		v.setCompteBeneficiaire(compteBeneficiaire);
		v.setCompteEmetteur(compteEmetteur);
		v.setDateExecution(new Date());
		v.setMotifVirement("Assignment 2021");*/

		virementRepository.save(virement);
		
		Versement versement = new Versement(BigDecimal.TEN, new Date(), "Rharnati Meryem", compteBeneficiaire, "test versement");
		
		versementRepository.save(versement);
	}
}
