package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class VersementDto {
	
	private String nom_prenom_emetteur;
	private String ribCompteBeneficiaire;
	private String motifVersement;
	private BigDecimal montantVersement;
	private Date date;
	
	public String getRibCompteBeneficiaire() {
		return ribCompteBeneficiaire;
	}
	
	public void setRibCompteBeneficiaire(String ribCompteBeneficiaire) {
		this.ribCompteBeneficiaire = ribCompteBeneficiaire;
	}
	
	public String getMotifVersement() {
		return motifVersement;
	}
	
	public void setMotifVersement(String motifVersement) {
		this.motifVersement = motifVersement;
	}
	
	public BigDecimal getMontantVersement() {
		return montantVersement;
	}
	
	public void setMontantVersement(BigDecimal montantVersement) {
		this.montantVersement = montantVersement;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String versementMessage() {
		return "Virement depuis " + this.ribCompteBeneficiaire + " d'un montant de " + this.montantVersement.toString();
	}
	
	public boolean isMotifBlank() {
		if(this.motifVersement.isEmpty() || this.motifVersement.isBlank()) {
			return true;
		}
		else
			return false;
	}

	public String getNom_prenom_emetteur() {
		return nom_prenom_emetteur;
	}

	public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
		this.nom_prenom_emetteur = nom_prenom_emetteur;
	}
	
	

}
