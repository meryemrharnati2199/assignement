package ma.octo.assignement.web;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.util.Constants;
import ma.octo.assignement.exceptions.TransactionException;

public abstract class TransactionController {

	Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);
	
	protected void checkMontantTransaction(BigDecimal montant) throws TransactionException {
    	if (isMontantNull(montant)) {
            LOGGER.error(Constants.MONTANT_VIDE);
            throw new TransactionException(Constants.MONTANT_VIDE);
        } else if (isMontantVide(montant)) {
        	LOGGER.error(Constants.MONTANT_VIDE);
            throw new TransactionException(Constants.MONTANT_VIDE);
        } else if (isMontantInfMinimal(montant)) {
        	LOGGER.error(Constants.MONTANT_MIN_NON_ATTEINT);
            throw new TransactionException(Constants.MONTANT_MIN_NON_ATTEINT);
        } else if (isMontantSupMaximal(montant)) {
        	LOGGER.error(Constants.MONTANT_MAX_DEPASSE);
            throw new TransactionException(Constants.MONTANT_MAX_DEPASSE);
        }
    }
	
	
	protected boolean isMontantNull(BigDecimal montant) {
    	return (montant.equals(null)) ? true : false;
    }
    
	protected boolean isMontantVide(BigDecimal montant) {
    	return (montant.intValue() == 0) ? true : false;
    }
    
	protected boolean isMontantInfMinimal(BigDecimal montant) {
    	return (montant.intValue() < Constants.MONTANT_MINIMAL) ? true : false;
    }
    
	protected boolean isMontantSupMaximal(BigDecimal montant) {
    	return (montant.intValue() > Constants.MONTANT_MAXIMAL) ? true : false;
    }
	
    
    protected boolean isCompteNull(Compte compte) {
    	if (compte == null) {
            return true;
        }
    	return false;
    }
    
}
