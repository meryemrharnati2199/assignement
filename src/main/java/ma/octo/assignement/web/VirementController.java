package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.domain.util.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
//import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/virements")
class VirementController extends TransactionController {

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private AuditService auditService;
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @GetMapping("lister_virements")
    List<Virement> loadAll() {
        List<Virement> all = virementRepository.findAll();

        /*if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }*/
        
        return (all.isEmpty())?  null : all;
    }

    @GetMapping("lister_comptes")
    List<Compte> loadAllCompte() {
        List<Compte> all = compteRepository.findAll();

        /*if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }*/
        
        return (all.isEmpty())?  null : all;
    }

    @GetMapping("lister_utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        List<Utilisateur> all = utilisateurRepository.findAll();

        /*if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }*/
        
        return (all.isEmpty())?  null : all;
    }

    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        
    	//changer le nom des variables
    	Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        
        //changer les sysout par le LOGGER
        if (isCompteNull(compteBeneficiaire) || isCompteNull(compteEmetteur)) {
            LOGGER.error(Constants.COMPTE_NON_EXISTANT);
            throw new CompteNonExistantException(Constants.COMPTE_NON_EXISTANT);
        }

        //tester sur le montant
        checkMontantTransaction(virementDto.getMontantVirement());


        if (virementDto.isMotifBlank()) {
            LOGGER.error(Constants.MOTIF_VIDE);
            throw new TransactionException(Constants.MOTIF_VIDE);
        }

        /*if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }*/

        if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error(Constants.SOLDE_INSUFFISANT);
            throw new SoldeDisponibleInsuffisantException(Constants.SOLDE_INSUFFISANT);
        }

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
        compteRepository.save(compteBeneficiaire);

        Virement virement = new Virement(virementDto.getDate(), compteBeneficiaire, compteEmetteur, virementDto.getMontantVirement());
        /*virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMontantVirement(virementDto.getMontantVirement());*/

        virementRepository.save(virement);

        auditService.auditVirement(virementDto.virementMessage());
        
        /*auditService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                        .toString());*/
        
        
    }
    
    

    //fonction non utilisée
    /*private void save(Virement Virement) {
        virementRepository.save(Virement);
    }*/
}
