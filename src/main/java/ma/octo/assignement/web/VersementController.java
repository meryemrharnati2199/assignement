package ma.octo.assignement.web;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.Constants;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AuditService;

@RestController(value = "/versements")
public class VersementController extends TransactionController{
	
	Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private AuditService auditService;

    @GetMapping("lister_versements")
    List<Versement> loadAll() {
        List<Versement> all = versementRepository.findAll();
        return (all.isEmpty())?  null : all;
    }


    @PostMapping("/executerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VersementDto versementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        
    	//changer le nom des variables
    	Compte compteBeneficiaire = compteRepository.findByRib(versementDto.getRibCompteBeneficiaire());

        
        //changer les sysout par le LOGGER
        if (isCompteNull(compteBeneficiaire)) {
            LOGGER.error(Constants.COMPTE_NON_EXISTANT);
            throw new CompteNonExistantException(Constants.COMPTE_NON_EXISTANT);
        }

        //tester sur le montant
        checkMontantTransaction(versementDto.getMontantVersement());

        if (versementDto.isMotifBlank()) {
            LOGGER.error(Constants.MOTIF_VIDE);
            throw new TransactionException(Constants.MOTIF_VIDE);
        }

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(compteBeneficiaire);

        Versement versement = new Versement(versementDto.getMontantVersement(), versementDto.getDate(), versementDto.getNom_prenom_emetteur(), compteBeneficiaire);

        versementRepository.save(versement);

        auditService.auditVersement(versementDto.versementMessage());
        
        
    }


}
