package ma.octo.assignement.domain.util;

public final class Constants {
	
	
	public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;

	
	public static final String COMPTE_NON_EXISTANT = "Compte Non existant";
	public static final String MONTANT_VIDE = "Montant vide";
	public static final String MONTANT_MIN_NON_ATTEINT = "Montant minimal de virement non atteint";
	public static final String MONTANT_MAX_DEPASSE = "Montant maximal de virement dépassé";
	public static final String MOTIF_VIDE = "Motif vide";
	public static final String SOLDE_INSUFFISANT = "Solde insuffisant pour l'utilisateur";
	

}
