package ma.octo.assignement.domain;

//imports non utilisés

//import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;

@Entity
@Table(name = "UTILISATEUR")
public class Utilisateur implements Serializable {
	/**
	 * régler le warning en ajoutant une UID
	 */
	private static final long serialVersionUID = -5430565277617332379L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 10, nullable = false, unique = true)
	private String username;

	@Column(length = 10, nullable = false)
	private String gender;

	@Column(length = 60, nullable = false)
	private String lastname;

	@Column(length = 60, nullable = false)
	private String firstname;

	@Temporal(TemporalType.DATE)
	private Date birthdate;


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	//definir les const avec et sans params:
	
	public Utilisateur() {
		
	}
	
	public Utilisateur(String username, String lastname, String firstname, String gender) {
		super();
		this.username = username;
		this.gender = gender;
		this.lastname = lastname;
		this.firstname = firstname;
	}
	
	public String getFullName() {
		return this.firstname + " " + this.lastname;
	}
	
}
