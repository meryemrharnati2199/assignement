package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {
	
	private static VersementDto versementDto;

    public static VersementDto map(Versement versement) {
        versementDto = new VersementDto();
        versementDto.setRibCompteBeneficiaire(versement.getCompteBeneficiaire().getRib());
        versementDto.setDate(versement.getDateExecution());
        versementDto.setMotifVersement(versement.getMotifVersement());
        versementDto.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());
        
        return versementDto;

    }

}
