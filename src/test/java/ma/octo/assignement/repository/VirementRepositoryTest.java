package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import ma.octo.assignement.domain.*;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

	@Autowired
	private VirementRepository virementRepository;
	@Autowired
	private CompteRepository compteRepository;

	@Test
	public void findOne() {
		
		Virement virement = virementRepository.findById((long)5).get();
		
		Example<Virement> example = Example.of(virement);
		
	    Optional<Virement> actual = virementRepository.findOne(example);
	    
	    assertThat(actual.get()).isEqualTo(example.getProbe());
	    
	}

	@Test
	public void findAll() {
		
		Virement virement = virementRepository.findById((long)5).get();
		
		List<Virement> allVirements = virementRepository.findAll();
		
		assertThat(allVirements).hasSize(1).contains(virement);
		
	}

	@Test
	public void save() {
		
		Compte compteEmetteur = compteRepository.findById((long)3).get();
		Compte compteBeneficiaire = compteRepository.findById((long)4).get();

		Virement virement = new Virement(new Date(), compteBeneficiaire, compteEmetteur, BigDecimal.TEN, "Test Unitaire");
		//Virement virement1 = new Virement(new Date(), compteBeneficiaire, compteEmetteur, BigDecimal.TEN, "Test Unitaire");
		virementRepository.save(virement);
		
		List<Virement> allVirements = virementRepository.findAll();
		
		assertThat(allVirements).contains(virement);
		//assertThat(allVirements).doesNotContain(virement1);
		
	}

	@Test
	public void delete() {
		
		Compte compteEmetteur = compteRepository.findById((long)3).get();
		Compte compteBeneficiaire = compteRepository.findById((long)4).get();

		Virement virement = new Virement(new Date(), compteBeneficiaire, compteEmetteur, BigDecimal.TEN, "Test Unitaire");
		virementRepository.save(virement);
		virementRepository.delete(virement);
		
		List<Virement> allVirements = virementRepository.findAll();
		
		assertThat(allVirements).doesNotContain(virement);
		
	}
}