package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VersementRepositoryTest {
	
	@Autowired
	private VersementRepository versementRepository;
	@Autowired
	private CompteRepository compteRepository;

	@Test
	public void findOne() {
		
		Versement versement = versementRepository.findById((long)6).get();
		
		Example<Versement> example = Example.of(versement);
		
	    Optional<Versement> actual = versementRepository.findOne(example);
	    
	    assertThat(actual.get()).isEqualTo(example.getProbe());
	    
	}

	@Test
	public void findAll() {
		
		Versement versement = versementRepository.findById((long)6).get();
		
		List<Versement> allVersements = versementRepository.findAll();
		
		assertThat(allVersements).hasSize(1).contains(versement);
		
	}

	@Test
	public void save() {
		
		Compte compteBeneficiaire = compteRepository.findById((long)4).get();

		Versement versement = new Versement(BigDecimal.TEN, new Date(), "Meryem Rharnati", compteBeneficiaire, "Test Versement");
		Versement versement1 = new Versement(BigDecimal.TEN, new Date(), "Meryem Rharnati", compteBeneficiaire, "Test Versement Unitaire");
		versementRepository.save(versement);
		
		List<Versement> allVersements = versementRepository.findAll();
		
		assertThat(allVersements).contains(versement);
		assertThat(allVersements).doesNotContain(versement1);
		
	}

	@Test
	public void delete() {
		
		Compte compteBeneficiaire = compteRepository.findById((long)4).get();

		Versement versement = new Versement(BigDecimal.TEN, new Date(), "Meryem Rharnati", compteBeneficiaire, "Test Versement");;
		versementRepository.save(versement);
		versementRepository.delete(versement);
		
		List<Versement> allVersements = versementRepository.findAll();
		
		assertThat(allVersements).doesNotContain(versement);
		
	}

}
